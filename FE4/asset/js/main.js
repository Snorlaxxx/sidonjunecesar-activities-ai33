(function($) {

	"use strict";

	var fullHeight = function() {

		$('.js-fullheight').css('height', $(window).height());
		$(window).resize(function(){
			$('.js-fullheight').css('height', $(window).height());
		});

	};
	fullHeight();

	$('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
  });

})(jQuery);



                                      window.onload = function() {
                                          new Chart(document.getElementById('chart-area').getContext('2d'), pieChartData);

                                          new Chart(document.getElementById('canvas').getContext('2d'), {
                                                  type: 'bar',
                                                  data: barChartData,
                                                  options: {
                                                          title: {
                                                                  display: true,
                                                                  text: 'Borrowed and Returned Books'
                                                          },
                                                          tooltips: {
                                                                  mode: 'index',
                                                                  intersect: false
                                                          },
                                                          responsive: true,
                                                          scales: {
                                                                  xAxes: [{
                                                                          stacked: true,
                                                                  }],
                                                                  yAxes: [{
                                                                          stacked: true
                                                                  }]
                                                          }
                                                  }
                                          });
                                      }


function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
                                 




