var users = [
    {
        id: 1,
        name: "Clean Architecture",
        Author: "Robert",
        copy: "5",
        category: "Computer Science",

    },
    {
        id: 2,
        name: "Data Structure and Algorithm",
        Author: "Devine Funcion",
        copy: "20",
        category: "Information Tech",
    },
	{
        id: 3,
        name: "Integrative Programming",
        Author: "Dalan",
        copy: "30",
        category: "Information Science",
    }
];

$("#overlay").hide();



$("#kape").click(function(){
    $("#overlay").toggle();
});

$("#closed").click(function(){
    $("#overlay").toggle();
});
$("#kope").click(function(){
    $("#overlay").toggle();
});

$.each(users, function (i, user) {
    appendToUsrTable(user);
});

$("form").submit(function (e) {
    e.preventDefault();
});
function deleteUser(id) {
    var action = confirm("Are you sure?");
    var msg = "Contact deleted successfully!";
    users.forEach(function (user, i) {
        if (user.id == id && action != false) {
            users.splice(i, 1);
            $("#userTable #user-" + user.id).remove();
            flashMessage(msg);
        }
    });
}


function flashMessage(msg) {
    $(".flashMsg").remove();
    $(".row").prepend(`
         
      `);
}

function appendToUsrTable(user) {
    $("#userTable > tbody:last-child").append(`
          <tr id="user-${user.id}" class="text-center">
              <td class="userData" name="name">${user.name}</td>
              '<td class="userData" name="email">${user.Author} </td>
              '<td class="userData" name="email">${user.copy} </td>
               '<td class="userData" name="email">${user.category}</td>
               '<td align="center">
                  <a href="#" class="text-success"><img src ="asset/images/edit.png" height="32" width="32" onClick="deleteUser(${user.id})"></a>
              </td>

              '<td align="center">
                  <a href="#" class="text-success"><img src ="asset/images/delete.png" height="32" width="32" onClick="deleteUser(${user.id})"></a>
              </td>
               '<td align="center">
                  <a href="#" class="text-success"><img src ="asset/images/adduser.png" height="32" width="32" </a>
              </td>
              '<td align="center">
                  <a href="#" class="text-success"><img src ="asset/images/return.png" height="32" width="32" </a>
              </td>
          </tr>
      `);
}
