export default {
  data() {
    return {
      Form: {
        Name: "",
        Author: "",
        CopiesAvailable: "",
        Category: "",
        BookDateRegistered: new Date(),
      },
      BookModel: [],
      SelectedBook: "",
      Search: "",
    };
  },
  methods: {
    AddBook() {
      this.BookModel.unshift(this.Form);
      console.log(this.form);
      this.HideModal();
    },
    UpdateBook(index) {
      console.log(index);
      this.BookModel.splice(index, 1, this.Form);

      this.HideModal();
    },
    ToBeUpdated(index) {
      this.SelectedBook = index;
      this.Form.Name = this.BookModel[index].Name;
      this.Form.Author = this.BookModel[index].Author;
      this.Form.CopiesAvailable = this.BookModel[index].CopiesAvailable;
      this.Form.Category = this.BookModel[index].Category;
    },
    DeleteBook(index) {
      this.BookModel.splice(index, 1);

      this.HideModal();
    },
    HideModal() {
      this.$refs.modal.hide();
      this.FormReset();
    },
    FormReset() {
      this.Form = {
        Name: "",
        Author: "",
        CopiesAvailable: "",
        Category: "",
        BookDateRegistered: new Date(),
      };

      this.SelectedBook = "";
    },
  },
  computed: {
    BookModelFilter() {
      return this.BookModel.filter((Book) => {
        return this.Search.toLowerCase()
          .split(" ")
          .every((v) => Book.Name.toLowerCase().includes(v));
      });
    },
  },
};