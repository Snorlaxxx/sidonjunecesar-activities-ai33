import Vue from "vue";
import router from "./routes/router";
import App from "../src/App.vue";

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import toastr from "toastr";
import VueFilterDateFormat from "@vuejs-community/vue-filter-date-format";
Vue.use(VueFilterDateFormat);

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
import './assets/css/sty.css'
import './assets/css/responsive.css'
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.min.css";

Vue.config.productionTip = false;
Vue.use(toastr);

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
