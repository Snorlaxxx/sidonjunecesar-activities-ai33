<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\borrowed_book;
use App\Models\returned_book;


class ReturnedBookControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $borrow = borrowed_book::where([
            ['book_id', $request->book_id],
            ['patron_id', $request->patron_id],
        ])->firstOrFail();
        
        if(!empty($borrow)){
            if($borrow->copies == $request->copies){
                $borrow->delete();
            }else{
                $borrow->update(['copies' => $borrow->copies - $request->copies]);
            }   
            $returned_data = returned_book::create($request->only(['book_id', 'copies', 'patron_id']));
            $returned = returnedBook::with(['book'])->find($returned_data->id);
            $copies = $returned->book->copies + $request->copies;
            $returned->book->update(['copies' => $copies]);
            return response()->json(['message' => 'Returned!']);
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $returnedbook = ReturnedBook::with(['book', 'book.category', 'patron'])->findOrfail($id);
        return response()->json($returnedbook); 
    }
}
