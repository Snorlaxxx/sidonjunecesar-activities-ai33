<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
class BookControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return response()->json(Book::with(['category:id,category'])->get(

        ));
    }

    /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $book = Book::create($request->all());
        
        return response()->json(['message' => 'Added Book', 'book' => $book]);
    }

    /**
     * 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
        
        return response()->json($book);
    }

    
    /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();

        $book->update($request->all());

        return response()->json(['message' => 'Updated Book', 'book' => $book]);
    }

    /**
     * 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::where('id', $id)->firstOrFail();

        $book->delete();

        return response()->json(['message' => 'Destroyed']);
    }
}
