<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'author', 'copies', 'category_id'];

    public function category(){
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function borrowed(){
        return $this->hasMany(borrowed_book::class, 'book_id', 'id');
    }

    public function returned(){
        return $this->hasMany(returned_book::class, 'book_id', 'id');
    }
}
