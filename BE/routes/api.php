<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Use App\Http\Controllers\CategoryControl; 
Use App\Http\Controllers\BookControl; 
Use App\Http\Controllers\PatronControl; 
use App\Http\Controllers\BorrowedBookControl;
use App\Http\Controllers\ReturnedBookControl;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/categories', [CategoryControl::class, 'index']);
Route::resource('books', BookControl::class)->only(['index', 'store', 'show', 'update', 'destroy']);
Route::resource('patrons', PatronControl::class)->only(['index', 'store', 'show', 'update', 'destroy']);
Route::get('/borrowed_book', [BorrowedBookControl::class, 'index']);
Route::get('/borrowed_book/{id}', [BorrowedBookControl::class, 'show']);
Route::get('/returned_book', [ReturnedBookControl::class, 'index']);
Route::get('/returned_book/{id}', [ReturnedBookControl::class, 'show']);
Route::post('/returned_book', [ReturnedBookControl::class, 'store']);
